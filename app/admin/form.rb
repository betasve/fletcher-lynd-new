ActiveAdmin.register Form do
	actions :all, :except => [:destroy, :new, :edit]

	menu priority: 1

	index do 
		column :first_name, sortable: :first_name
		column :last_name, sortable: :last_name
		column :email, sortable: :email 
		column :created_at, sortable: :created_at do |form|
			form.created_at.strftime("%d %h %Y %H:%M")
		end
		actions
	end

	show do |form|
		attributes_table do
			row :id
			row :first_name
			row :last_name
			row :fathers_name
			row :birthday
			row :place_of_birth
			row :mobile
			row :skype
			row :university
			row :year_of_study
			row :email
			row :fletcher_office
			row :wandt
			row :selfarranged
			row :created_at
		end
	end

	controller do
		def recent(num = 5)
			Form.last(5)
		end
	end

	permit_params :last_name, :first_name, :fathers_name, :birthday, :place_of_birth, :mobile, :skype, :university, :year_of_study, :email, :fletcher_office, :wandt, :selfarranged
end