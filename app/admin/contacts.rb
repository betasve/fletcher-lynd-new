ActiveAdmin.register Contact do
	actions :all, :except => [:destroy, :new, :edit]

	menu priority: 5

	index do 
		column :names, sortable: :names do |contact|
			link_to contact.names, admin_contact_path(contact)
		end
		column :email, sortable: :email
		column :message
		actions
	end

	show do |contact|
		attributes_table do
			row :id
			row :names
			row :email
			row :message
			row :created_at
		end
	end

	permit_params :names, :email, :message
end