ActiveAdmin.register Offer do

  menu priority: 3

    index do
        column :visible
        column :offer_order, sortable: :offer_order
        column :title_en, sortable: :title_en do |offer|
          link_to offer.title_en, admin_offer_path(offer)
        end
        column :locations_en
        column :created_at, sortable: :created_at do |offer|
          offer.created_at.strftime("%d %h %Y %H:%M")
        end
        column :updated_at, sortable: :updated_at do |offer|
          offer.updated_at.strftime("%d %h %Y %H:%M")
        end
        actions
    end

    form do |f|
        f.inputs do
        f.input :title_en
        f.input :title_bg
        f.input :visible, label: "Visible on homepage"
        f.input :offer_order
        if f.object.banner.present?
            f.input :banner, as: :file, hint: f.template.image_tag(f.object.banner.url(:thumb))
        else
            f.input :banner, as: :file, hint: "Only files with extension .jpg, .jpeg, .gif and .png are allowed."
        end
        f.input :locations_en
        f.input :locations_bg
        f.input :description_en
        f.input :description_bg
        f.input :body_en, :input_html => { :class => "tinymce", :id => "body_en" }
        f.input :body_bg, :input_html => { :class => "tinymce", :id => "body_bg" }
        f.input :slug
        end
        f.actions
    end

  show do |form|
    attributes_table do
      row :id
      row :title_en
      row :title_bg
      row :offer_order
      row :visible, label: 'Visible on homepage'
      row :banner do
        image_tag(form.banner.thumb) if form.banner.present?
      end
      row :locations_en
      row :locations_bg
      row :description_en
      row :description_bg
      row :body_bg do
        simple_format form.body_bg.html_safe
      end
      row :body_en do
        form.body_en.html_safe
      end
      row :slug
      row :created_at
      row :updated_at
    end
   end
    permit_params :title_en, :title_bg, :locations_en, :locations_bg, :description_en, :description_bg, :body_en, :body_bg, :visible, :banner, :offer_order
end