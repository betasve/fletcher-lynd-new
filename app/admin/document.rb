#ActiveAdmin.register Document do
#	#actions :all, :except => [:destroy, :new, :edit]
#
#	index do 
#		column :name, sortable: :name do |document|
#			link_to document.name, admin_document_path(document)
#		end
#		column :doc_type, sortable: :doc_type do |document|
#			link_to document.doc_type, admin_document_path(document)
#		end
#		column :static_page, sortable: :static_page do |document|
#			link_to document.static_page.title, admin_document_path(document)
#		end
#		column :created_at, sortable: :created_at do |document|
#			link_to document.created_at, admin_document_path(document)
#   end
#	end

#	form :multipart => true do |f| #:html => { :enctype => "multipart/form-data" } 
#    f.inputs  do
#      f.input :name
#      f.input :file, :as => :file
#      f.input :static_page_id, :label => 'Parent Static Page', :as => :select, :collection => StaticPage.all.map{|sp| ["#{sp.title}", sp.id]}
#    end
#    f.actions
#  end

#  show do |form|
#    attributes_table do
#      row :id
#      row :name
#      row :location
#      row :doc_type
#      row :static_page
#    end
#  end

#		permit_params :name, :static_page_id, :location, :file
#end