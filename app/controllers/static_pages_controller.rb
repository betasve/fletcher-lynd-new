class StaticPagesController < ApplicationController
    before_action :set_locale
    def show
        begin
            @page = StaticPage.friendly.find(params[:friendly_id])
        rescue ActiveRecord::RecordNotFound
            render(:partial => 'not_found', :status => :not_found)
        end
    end
end