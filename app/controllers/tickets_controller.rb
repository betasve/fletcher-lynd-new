class TicketsController < ApplicationController
	before_action :set_locale
	
	def new
		@formd = Settings.where(setting_name: 'Ticket form description').first
		@ticket = Ticket.new
	end

	def create
		@formd = Settings.where(setting_name: 'Ticket form description').first
		@ticket = Ticket.new(ticket_params)

		if @ticket.valid_with_captcha?
			@ticket.save
			TicketMailer.ticket_email(@ticket).deliver
			TicketMailer.ticket_to_sender(@ticket).deliver
			flash[:notice] = I18n.t(:ticket_success)
			redirect_to root_path
		else
			render 'new'
		end
	end

	private 
	def ticket_params
		params.require(:ticket).permit(:first_name, :last_name, :birthday, :passport_num, :passport_valid_date, :email, :mobile, :destination, :departure_date, :comeback_date, :airlines, :arrival_time, :comments, :captcha, :captcha_key)
	end
end