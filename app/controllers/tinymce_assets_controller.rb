class TinymceAssetsController < ApplicationController

  def create
    # Take upload from params[:file] and store it somehow...
    # Optionally also accept params[:hint] and consume if needed
    dirname = 'tinymce_assets'
    name = params[:file].original_filename

    Dir.mkdir("public/images/") unless Dir.exists?("public/images/")
	  Dir.mkdir("public/images/#{dirname}") unless Dir.exists?("public/images/#{dirname}")
	  filepath = "public/images/#{dirname}"

    path = File.join(filepath, name)
    File.open(path, "wb") { |f| f.write(params[:file].read) }
    image = dirname + '/' + name

    render json: {
      image: {
        url: view_context.image_url(image)
      }
    }, content_type: "text/html"
  end
end