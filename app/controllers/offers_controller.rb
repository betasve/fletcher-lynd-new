class OffersController < ApplicationController
    before_action :set_locale
    def show
        begin
            @page = Offer.friendly.find(params[:friendly_id])
        rescue ActiveRecord::RecordNotFound
            render(:partial => 'static_pages/not_found', :status => :not_found)
        end
    end
end