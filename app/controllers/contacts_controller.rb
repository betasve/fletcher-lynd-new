class ContactsController < ApplicationController
	before_action :set_locale

	def new
		@formd = Settings.where(setting_name: 'Contact form description').first
		@contact = Contact.new
	end

	def create
		@formd = Settings.where(setting_name: 'Contact form description').first
		@contact = Contact.new(form_params)

		if @contact.valid_with_captcha?
			@contact.save
			FormMailer.contact_email(@contact).deliver
			flash[:notice] = t(:contact_form_success)
			redirect_to root_path
		else
			render 'new'
		end
	end

	private 
	def form_params
		params.require(:contact).permit(:names, :email, :message, :captcha, :captcha_key, :captcha, :captcha_key)
	end
end