class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :get_static_pages, :set_locale

  helper_method :generate_forwarded_link

  def get_static_pages
    @pages = StaticPage.where(in_main_menu: true)
  end

  def default_url_options(options={})
    logger.debug "default_url_options is passed options #{options.inspect}\n"
    { locale: I18n.locale}
  end

  def set_locale
    if params[:locale].present? and params[:locale].match("en|bg")
      I18n.locale = params[:locale]
    else
      I18n.locale = 'bg'
    end
  end

  def generate_forwarded_link(page)
        locale = params[:locale] || 'bg'
        "#{locale}/#{page.forwards_to}/new"
  end
end
