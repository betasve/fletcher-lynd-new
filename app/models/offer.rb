class Offer < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title_en, :use => [:slugged, :finders]
	translates :title, :description, :locations, :body

	mount_uploader :banner, BannerUploader

	validates :title_en, presence: true, uniqueness: true
	validates :title_bg, presence: true, uniqueness: true
	validates :description_en, presence: true
	validates :description_bg, presence: true
	validates :body_en, presence: true
	validates :body_bg, presence: true
	validates :slug, presence: true, uniqueness: true
end
