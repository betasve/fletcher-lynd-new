class StaticPage < ActiveRecord::Base

	extend FriendlyId
	friendly_id :title_en, :use => [:slugged, :finders]
	translates :title, :body, :banner_caption, :banner_description

	mount_uploader :banner, BannerUploader
	
	validates :title_en, presence: true, uniqueness: true
	validates :title_bg, presence: true, uniqueness: true
	validates :body_en, presence: true
	validates :body_bg, presence: true
	validates :slug, presence: true, uniqueness: true
end
