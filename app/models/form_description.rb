class FormDescription < ActiveRecord::Base
	translates :body

	validates :body_en, presence: true
	validates :body_bg, presence: true
end
