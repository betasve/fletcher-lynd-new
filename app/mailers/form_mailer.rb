class FormMailer < ActionMailer::Base
  default from: "no-response@fletcherlynd.com"

  def application_email(applicant)
  	@applicant = applicant

  	mail(to: @applicant.email, subject: "Successful application")
  end

  def application_email_to_employee(applicant)
    @a = applicant

    if @a.fletcher_office == "Sofia"
      receiver = "info@fletcherlynd.com"
    else
      receiver = "blagoevgrad@fletcherlynd.com"
    end

    mail(to: receiver, subject: "#{@a.first_name} #{@a.last_name} попълни формата за кандидатстване във FletcherLynd")
  end

  def contact_email(contact)
    @c = contact

    mail(to: 'info@fletcherlynd.com', subject: "#{@c.names} изпрати запитване през контактната форма на FletcherLynd")
  end

  def contact_email_to_sender(contact)
    @c = contact

    if @c.email
      mail(to: @c.mail, subject: "Изпратено съобщение през формата за контакцт на Fletcher Lynd")
    end
  end
end
