class TicketMailer < ActionMailer::Base
  default from: "no-response@fletcherlynd.com"

  def ticket_email(applicant)
  	@a = applicant
    receivers = ['info@fletcherlynd.com', 'air@fletcherlynd.com', 'air@lidiatours.com']
    mail(to: receivers, subject: "#{@a.first_name} #{@a.last_name} попълни формата за самолетни билети в Fletcher Lynd")  	
  end

  def ticket_to_sender(applicant)
  	@a = applicant
  	receivers = @a.email
  	mail(to: receivers, subject: "Вие успешно попълнихте формата за резервация на самолетен билет за програмата Work and Travel на Fletcher Lynd")
  end
end
