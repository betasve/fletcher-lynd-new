class DeleteTrainingFromForm < ActiveRecord::Migration
  def self.up
  	remove_column :forms, :training
  end

  def self.down
  	add_column :forms, :training, :boolean
  end
end
