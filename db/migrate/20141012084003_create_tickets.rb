class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :last_name
      t.string :first_name
      t.date :birth_date
      t.string :passport_num
      t.date :passport_valid_date
      t.string :email
      t.string :gsm
      t.string :destination
      t.date :departure_date
      t.date :comeback_date
      t.string :airlines
      t.string :arrival_time

      t.timestamps
    end
  end
end
