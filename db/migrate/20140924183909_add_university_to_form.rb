class AddUniversityToForm < ActiveRecord::Migration
	def self.up
		add_column :forms, :university_id, :integer
	end

	def self.down
		remove_column :forms, :university_id
	end
end
