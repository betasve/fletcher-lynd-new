class AddVisibleToStaticPage < ActiveRecord::Migration
  def change
  	add_column :static_pages, :in_main_menu, :boolean
  end
end
