class AddForwardsToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :forwards_to, :string, default: nil
  end
end
