class AddImageFiledsToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :banner, :string
    add_column :static_pages, :banner_caption_bg, :string
    add_column :static_pages, :banner_caption_en, :string
    add_column :static_pages, :banner_description_bg, :string
    add_column :static_pages, :banner_description_en, :string
  end
end
