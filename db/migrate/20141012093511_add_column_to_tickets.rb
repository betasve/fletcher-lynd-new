class AddColumnToTickets < ActiveRecord::Migration
  def self.up
  	add_column :tickets, :comments, :string
  end

  def self.down
  	remove_column :tickets, :comments
  end
end
