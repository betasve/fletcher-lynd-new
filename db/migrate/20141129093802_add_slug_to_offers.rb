class AddSlugToOffers < ActiveRecord::Migration
  def change
  	add_column :offers, :slug, :text
  end
end
