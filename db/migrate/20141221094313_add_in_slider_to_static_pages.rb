class AddInSliderToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :in_slider, :boolean, default: true
  end
end
