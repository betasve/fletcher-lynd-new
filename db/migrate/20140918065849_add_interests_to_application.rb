class AddInterestsToApplication < ActiveRecord::Migration
  def change
  	add_column :applications, :wandt, :boolean
  	add_column :applications, :selfarranged, :boolean
  	add_column :applications, :training, :boolean
  end
end
