class RenameApplicationToForm < ActiveRecord::Migration
  def self.up
  	rename_table :applications, :forms
  end

  def self.down
  	rename_table :forms, :applications
  end
end
